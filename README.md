This is just a collection of bat and cmd files for windows, that help to make windows machine looks more like linux.

Please, specify the following environment variables:
1) BATS_HOME - path to this repository
2) Add BATS_HOME to the PATH variable.
3) CHROME_HOME - path to the chrome browser exe files
4) REPO_HOME - where all projects are located
5) VBOX_HOME - path to virtualbox folder
6) TOOLS_HOME - where all software is located
7) MVN_HOME - where apache maven is located (and to the Path)

Optional:
- GRADLE_HOME (%TOOLS_HOME%\gradle) (and add %GRADLE_HOME%\bin to PATH variable)
- H2_HOME (%TOOLS_HOME%\h2database) (and add %H2_HOME%\bin to PATH variable)
- ORACLE_HOME (and add %ORACLE_HOME%\bin to PATH variable)
- NPP_HOME (path to notepad++) %TOOLS_HOME%\notepad++
- GO_HOME (path to golang directory) - %TOOLS_HOME%\go (and add %GO_HOME%\bin to the PATH variable)
- CYGWIN_HOME (path to current cygwin home (x64/86))
